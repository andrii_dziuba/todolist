create table todo_entry (
    id integer not null auto_increment,
    completed tinyint(1),
    creation_date datetime(6),
    text varchar(255),
    primary key (id)
) engine=InnoDB;