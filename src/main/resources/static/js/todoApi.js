const url = 'http://localhost:8080/todo/'

export default {
    getAllTodos: () => {
        return fetch(url, {
            method: 'GET'
        })
    },
    getTodoById: (id) => {
        return fetch(url + id, {
            method: 'GET'
        })
    },
    saveNewTodo: (text) => {
        return fetch(url, {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({text})
        })
    },
    updateTodo: (id, text, completed) => {
        return fetch(url + id, {
            method: 'PATCH',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({text, completed})
        })
    }
}