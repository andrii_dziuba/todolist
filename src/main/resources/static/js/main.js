import Vue from 'vue'
import VueResource from 'vue-resource'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

import App from 'pages/App.vue'

Vue.use(VueResource)
Vue.use(Vuetify)

let app = new Vue({
    el: '#app',
    render: a => a(App)
});