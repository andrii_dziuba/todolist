package com.dziuba.todo.aspect;

import lombok.extern.log4j.Log4j;
import lombok.extern.log4j.Log4j2;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Log4j2
public class FirstAspect {

    @AfterReturning(value = "execution(* com.dziuba.todo.controller.TodoController.getAllEntries(..))", returning = "result")
    public void around(JoinPoint joinPoint, Object result) {
        log.debug("From aspect: " + this);
        System.out.println(result);
    }
}
