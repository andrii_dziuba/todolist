package com.dziuba.todo.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

    @Value("${application.devMode}")
    private boolean devMode;

    @GetMapping
    public String hello(Model model) {
        model.addAttribute("message", "Hello from MainController!!!");
        model.addAttribute("devMode", devMode);
        return "index";
    }
}
