package com.dziuba.todo.controller;

import com.dziuba.todo.model.TodoEntry;
import com.dziuba.todo.service.TodoEntryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todo")
public class TodoController {

    private TodoEntryService todoEntryService;

    @Autowired
    public TodoController(TodoEntryService todoEntryService) {
        this.todoEntryService = todoEntryService;
    }

    @GetMapping
    public List<TodoEntry> getAllEntries() {
        return todoEntryService.getAllEntries();
    }

    @GetMapping("{id}")
    public TodoEntry getEntryById(@PathVariable Integer id) {
        return todoEntryService.getById(id);
    }

    @PostMapping
    public TodoEntry createEntry(@RequestBody TodoEntry newEntry) {
        TodoEntry created = todoEntryService.create(newEntry);
        return created;
    }

    @PutMapping("{id}")
    public TodoEntry putEntry(@PathVariable Integer id, @RequestBody TodoEntry updateEntry) {
        TodoEntry updated = todoEntryService.put(id, updateEntry);
        return updated;
    }

    @PatchMapping("{id}")
    public TodoEntry patchEntry(@PathVariable("id") TodoEntry byId, @RequestBody TodoEntry updateEntry) {
        TodoEntry updated = todoEntryService.patch(byId, updateEntry);
        return updated;
    }

    @DeleteMapping
    public void deleteEntry(@PathVariable Integer id) {
        todoEntryService.delete(id);
    }

}
