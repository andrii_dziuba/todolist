package com.dziuba.todo;

import com.dziuba.todo.aspect.FirstAspect;
import com.dziuba.todo.controller.TodoController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;

@SpringBootApplication
//@Import(FirstAspect.class)
public class TodoApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(TodoApplication.class, args);
    }

}
