package com.dziuba.todo.service;

import com.dziuba.todo.dao.TodoEntryDAO;
import com.dziuba.todo.exception.NotFoundException;
import com.dziuba.todo.model.TodoEntry;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.ReflectionUtils;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.List;

@Service
public class TodoEntryService {

    private final TodoEntryDAO todoEntryDAO;

    @Autowired
    public TodoEntryService(TodoEntryDAO todoEntryDAO) {
        this.todoEntryDAO = todoEntryDAO;
    }

    public List<TodoEntry> getAllEntries() {
        return todoEntryDAO.findAll();
    }

    public TodoEntry getById(Integer id) {
        TodoEntry todoEntry = getAllEntries().stream()
                .filter(i -> i.getId().equals(id))
                .findFirst()
                .orElseThrow(NotFoundException::new);
        return todoEntry;
    }

    public TodoEntry create(TodoEntry newEntry) {
        todoEntryDAO.save(newEntry);
        return newEntry;
    }

    public TodoEntry put(Integer id, TodoEntry updateEntry) {
        TodoEntry byId = getById(id);
        BeanUtils.copyProperties(updateEntry, byId, "id", "creationDate");
        todoEntryDAO.save(byId);
        return byId;
    }

    public TodoEntry patch(TodoEntry byId, TodoEntry patchEntry) {
        byId.setCompleted(patchEntry.getCompleted());
        byId.setText(patchEntry.getText());
        todoEntryDAO.save(byId);
        return byId;
    }

    public void delete(Integer id) {
        TodoEntry byId = getById(id);
        todoEntryDAO.delete(byId);
    }

}
