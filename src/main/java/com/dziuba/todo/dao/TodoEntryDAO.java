package com.dziuba.todo.dao;

import com.dziuba.todo.model.TodoEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TodoEntryDAO extends JpaRepository<TodoEntry, Integer> {
}
