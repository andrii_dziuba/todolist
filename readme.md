Project is ***UNDER CONSTRUCTION :D***

***Used Technologies***  
`Backend stack`:  
Java 8  
Spring ecosystem (Boot, Data JPA, AOP, MVC)  
Lombok  
`Frontend stack`:  
JavaScript  
Vue.JS ecosystem (Vue Router, Vue Resource, Vuetify)  
`Database stack`:  
MySQL  
FlyWay  
